EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Pinout PCB AstreLoon"
Date "30/01/2020"
Rev "1"
Comp "ASTRE"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L Connector:Conn_01x16_Female J2
U 1 1 5E2A7235
P 2400 2000
F 0 "J2" H 2292 975 50  0001 C CNN
F 1 "Conn_01x16_Female" H 2292 1066 50  0001 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 2400 2000 50  0001 C CNN
F 3 "~" H 2400 2000 50  0001 C CNN
	1    2400 2000
	-1   0    0    1   
$EndComp
Text GLabel 2950 1200 2    50   Input ~ 0
RAW
Text GLabel 2950 1400 2    50   Input ~ 0
RST
Text GLabel 2950 1300 2    50   Input ~ 0
GND
Text GLabel 2950 1500 2    50   Input ~ 0
VCC
Text GLabel 2950 1600 2    50   Input ~ 0
A3
Text GLabel 2950 1700 2    50   Input ~ 0
A2
Text GLabel 2950 1800 2    50   Input ~ 0
A1
Text GLabel 2950 1900 2    50   Input ~ 0
A0
Text GLabel 2950 2000 2    50   Input ~ 0
SCK
Text GLabel 2950 2100 2    50   Input ~ 0
MISO
Text GLabel 2950 2200 2    50   Input ~ 0
MOSI
Text GLabel 2950 2300 2    50   Input ~ 0
SS
Text GLabel 2950 2400 2    50   Input ~ 0
SCL
Text GLabel 2950 2500 2    50   Input ~ 0
SDA
Text GLabel 2950 2600 2    50   Input ~ 0
A7
Text GLabel 2950 2700 2    50   Input ~ 0
A6
Wire Wire Line
	2600 2700 2950 2700
Wire Wire Line
	2600 2600 2950 2600
Wire Wire Line
	2600 2500 2950 2500
Wire Wire Line
	2950 2400 2600 2400
Wire Wire Line
	2600 2300 2950 2300
Wire Wire Line
	2600 2200 2950 2200
Wire Wire Line
	2600 2100 2950 2100
Wire Wire Line
	2600 2000 2950 2000
Wire Wire Line
	2950 1900 2600 1900
Wire Wire Line
	2600 1800 2950 1800
Wire Wire Line
	2600 1700 2950 1700
Wire Wire Line
	2950 1600 2600 1600
Wire Wire Line
	2600 1500 2950 1500
Wire Wire Line
	2950 1400 2600 1400
Wire Wire Line
	2600 1300 2950 1300
Wire Wire Line
	2950 1200 2600 1200
$Comp
L Connector:Conn_Coaxial J?
U 1 1 5E4681BB
P 5950 1800
F 0 "J?" H 6050 1775 50  0001 L CNN
F 1 "Conn_Coaxial" H 6050 1728 50  0001 L CNN
F 2 "" H 5950 1800 50  0001 C CNN
F 3 " ~" H 5950 1800 50  0001 C CNN
	1    5950 1800
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5E46E558
P 2200 900
F 0 "C?" H 2315 947 50  0001 L CNN
F 1 "C" H 2315 854 50  0001 L CNN
F 2 "" H 2238 750 50  0001 C CNN
F 3 "~" H 2200 900 50  0001 C CNN
	1    2200 900 
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1500 1200 1800 1200
Wire Wire Line
	1500 2600 1800 2600
Wire Wire Line
	1500 2500 1800 2500
Wire Wire Line
	1800 2400 1500 2400
Wire Wire Line
	1500 2300 1800 2300
Wire Wire Line
	1500 2200 1800 2200
Wire Wire Line
	1500 2100 1800 2100
Wire Wire Line
	1500 2000 1800 2000
Wire Wire Line
	1500 1900 1800 1900
Wire Wire Line
	1500 1800 1800 1800
Wire Wire Line
	1500 1700 1800 1700
Wire Wire Line
	1500 1600 1800 1600
Wire Wire Line
	1500 1500 1800 1500
Wire Wire Line
	1500 1400 1800 1400
Wire Wire Line
	1500 1300 1800 1300
Text GLabel 1500 2600 0    50   Input ~ 0
D8
Text GLabel 1500 2500 0    50   Input ~ 0
D7
Text GLabel 1500 2400 0    50   Input ~ 0
D6
Text GLabel 1500 2300 0    50   Input ~ 0
D5
Text GLabel 1500 2200 0    50   Input ~ 0
D4
Text GLabel 1500 2100 0    50   Input ~ 0
D3
Text GLabel 1500 2000 0    50   Input ~ 0
D2
Text GLabel 1500 1900 0    50   Input ~ 0
GND
Text GLabel 1500 1800 0    50   Input ~ 0
RST
Text GLabel 1500 1700 0    50   Input ~ 0
RXI
Text GLabel 1500 1600 0    50   Input ~ 0
TX0
Text GLabel 1500 1500 0    50   Input ~ 0
GND
Text GLabel 1500 1400 0    50   Input ~ 0
5V
Text GLabel 1500 1200 0    50   Input ~ 0
3.3V
Text GLabel 1500 1300 0    50   Input ~ 0
GND
$Comp
L Astreloon_Carte_Radio_GPS:L80-r U?
U 1 1 5E45902E
P 5600 2400
F 0 "U?" H 5600 2050 50  0001 C CNN
F 1 "L80-r" H 5625 2786 50  0000 C CNN
F 2 "" H 5600 2050 50  0001 C CNN
F 3 "" H 5600 2050 50  0001 C CNN
	1    5600 2400
	1    0    0    -1  
$EndComp
$Comp
L Astreloon_Carte_Radio_GPS:RFM69HCW U?
U 1 1 5E45831A
P 4750 1500
F 0 "U?" H 4700 1050 50  0001 C CNN
F 1 "RFM69HCW" H 4750 1986 50  0000 C CNN
F 2 "" H 4700 1050 50  0001 C CNN
F 3 "" H 4700 1050 50  0001 C CNN
	1    4750 1500
	1    0    0    -1  
$EndComp
Wire Wire Line
	1750 900  2050 900 
Text GLabel 1750 900  0    50   Input ~ 0
3.3V
Wire Wire Line
	2650 900  2350 900 
Text GLabel 2650 900  2    50   Input ~ 0
GND
Text GLabel 4000 1850 0    50   Input ~ 0
GND
Wire Wire Line
	5950 1300 5950 1600
Text GLabel 5950 1300 1    50   Input ~ 0
GND
Wire Wire Line
	5000 2650 5300 2650
Text GLabel 5000 2650 0    50   Input ~ 0
GND
Wire Wire Line
	6250 2350 5950 2350
Text GLabel 6250 2350 2    50   Input ~ 0
3.3V
Text GLabel 5300 1450 2    50   Input ~ 0
3.3V
Wire Wire Line
	6250 2250 5950 2250
Text GLabel 6250 2250 2    50   Input ~ 0
3.3V
Text GLabel 4000 1150 0    50   Input ~ 0
GND
$Comp
L Device:R R?
U 1 1 5E490EA2
P 4000 1650
F 0 "R?" V 3790 1650 50  0000 C CNN
F 1 "R" V 3883 1650 50  0000 C CNN
F 2 "" V 3930 1650 50  0001 C CNN
F 3 "~" H 4000 1650 50  0001 C CNN
	1    4000 1650
	0    1    1    0   
$EndComp
$Comp
L Device:R R?
U 1 1 5E49538C
P 5000 2450
F 0 "R?" V 4790 2450 50  0000 C CNN
F 1 "R" V 4883 2450 50  0000 C CNN
F 2 "" V 4930 2450 50  0001 C CNN
F 3 "~" H 5000 2450 50  0001 C CNN
	1    5000 2450
	0    1    1    0   
$EndComp
Wire Wire Line
	4150 1650 4350 1650
Wire Wire Line
	5150 2450 5300 2450
$Comp
L Connector:Conn_01x16_Female J1
U 1 1 5E2A5DC5
P 2000 1900
F 0 "J1" H 2028 1876 50  0001 L CNN
F 1 "Conn_01x16_Female" H 2028 1785 50  0001 L CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x16_P2.54mm_Vertical" H 2000 1900 50  0001 C CNN
F 3 "~" H 2000 1900 50  0001 C CNN
	1    2000 1900
	1    0    0    -1  
$EndComp
Text GLabel 1500 2700 0    50   Input ~ 0
D9
Wire Wire Line
	1500 2700 1800 2700
Text GLabel 4550 2450 0    50   Input ~ 0
D9
Wire Wire Line
	4550 2450 4850 2450
Text GLabel 3700 1650 0    50   Input ~ 0
D8
Wire Wire Line
	6250 2450 5950 2450
Text GLabel 6250 2450 2    50   Input ~ 0
GND
Text GLabel 5300 1750 2    50   Input ~ 0
GND
Wire Wire Line
	6250 2550 5950 2550
Text GLabel 6250 2550 2    50   Input ~ 0
RXI
Wire Wire Line
	6250 2650 5950 2650
Text GLabel 6250 2650 2    50   Input ~ 0
TX0
Text GLabel 5300 1850 2    50   Input ~ 0
ANTco
Text GLabel 6450 1800 2    50   Input ~ 0
ANTco
Wire Wire Line
	6150 1800 6450 1800
Text GLabel 4000 1550 0    50   Input ~ 0
SS
Wire Wire Line
	4350 1550 4000 1550
Text GLabel 4000 1450 0    50   Input ~ 0
SCK
Wire Wire Line
	4350 1450 4000 1450
Text GLabel 4000 1350 0    50   Input ~ 0
MISO
Wire Wire Line
	4350 1350 4000 1350
Text GLabel 4000 1250 0    50   Input ~ 0
MOSI
Wire Wire Line
	4350 1250 4000 1250
Text GLabel 4050 2400 1    50   Input ~ 0
EXT5
Text GLabel 3950 2400 1    50   Input ~ 0
EXT4
Text GLabel 3850 2400 1    50   Input ~ 0
EXT3
Text GLabel 3750 2400 1    50   Input ~ 0
EXT2
Text GLabel 3550 2400 1    50   Input ~ 0
EXT0
$Comp
L Connector:Conn_01x06_Male J?
U 1 1 5E469040
P 3750 2750
F 0 "J?" V 3859 2360 50  0001 R CNN
F 1 "Conn_01x06_Male" H 3859 3043 50  0001 C CNN
F 2 "" H 3750 2750 50  0001 C CNN
F 3 "~" H 3750 2750 50  0001 C CNN
	1    3750 2750
	0    -1   -1   0   
$EndComp
Wire Wire Line
	3550 2400 3550 2550
Wire Wire Line
	3650 2550 3650 2400
Wire Wire Line
	3750 2400 3750 2550
Wire Wire Line
	3850 2400 3850 2550
Wire Wire Line
	3950 2400 3950 2550
Wire Wire Line
	4050 2400 4050 2550
Text GLabel 5300 1350 2    50   Input ~ 0
EXT0
Wire Wire Line
	5300 1350 5150 1350
Text GLabel 3650 2400 1    50   Input ~ 0
EXT1
Wire Wire Line
	5150 1250 5300 1250
Text GLabel 5300 1250 2    50   Input ~ 0
EXT1
Text GLabel 5300 1150 2    50   Input ~ 0
EXT2
Wire Wire Line
	5300 1150 5150 1150
Text GLabel 5300 1650 2    50   Input ~ 0
EXT3
Wire Wire Line
	5300 1650 5150 1650
Text GLabel 5300 1550 2    50   Input ~ 0
EXT4
Wire Wire Line
	5300 1550 5150 1550
Text GLabel 4000 1750 0    50   Input ~ 0
EXT5
Wire Wire Line
	5150 1450 5300 1450
Wire Wire Line
	4000 1150 4350 1150
Wire Wire Line
	3850 1650 3700 1650
Wire Wire Line
	4000 1850 4350 1850
Wire Wire Line
	4000 1750 4350 1750
Wire Wire Line
	5150 1750 5300 1750
Wire Wire Line
	5150 1850 5300 1850
$EndSCHEMATC
