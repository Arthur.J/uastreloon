EESchema Schematic File Version 4
LIBS:pcb-cache
EELAYER 29 0
EELAYER END
$Descr A4 8268 11693 portrait
encoding utf-8
Sheet 1 1
Title "Pinout PCB AstreLoon"
Date "30/01/2020"
Rev "1"
Comp "ASTRE"
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
Text Notes 3250 950  0    315  ~ 63
Pinout
Wire Notes Line
	7500 2600 600  2600
Text Notes 2500 1200 0    50   ~ 0
FRONT
Wire Notes Line
	4150 2600 4150 1100
Text Notes 4750 1300 0    50   ~ 0
BACK\n\n
$Comp
L power:+3.3V #PWR0101
U 1 1 5E6BF876
P 850 1350
F 0 "#PWR0101" H 850 1200 50  0001 C CNN
F 1 "+3.3V" V 865 1478 50  0000 L CNN
F 2 "" H 850 1350 50  0001 C CNN
F 3 "" H 850 1350 50  0001 C CNN
	1    850  1350
	0    -1   -1   0   
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5E6C01F2
P 2000 2450
F 0 "#PWR0102" H 2000 2200 50  0001 C CNN
F 1 "GND" V 2005 2322 50  0000 R CNN
F 2 "" H 2000 2450 50  0001 C CNN
F 3 "" H 2000 2450 50  0001 C CNN
	1    2000 2450
	0    -1   -1   0   
$EndComp
$Comp
L power:+5V #PWR0103
U 1 1 5E6C069B
P 800 2450
F 0 "#PWR0103" H 800 2300 50  0001 C CNN
F 1 "+5V" V 815 2578 50  0000 L CNN
F 2 "" H 800 2450 50  0001 C CNN
F 3 "" H 800 2450 50  0001 C CNN
	1    800  2450
	0    -1   -1   0   
$EndComp
Wire Wire Line
	1250 1350 850  1350
Wire Wire Line
	1250 2450 800  2450
Wire Wire Line
	1750 2450 2000 2450
Text GLabel 800  1450 0    50   Input ~ 0
SCL
Text GLabel 800  1550 0    50   Input ~ 0
SDA
Text GLabel 800  1650 0    50   Input ~ 0
A0
Text GLabel 800  1750 0    50   Input ~ 0
A1
Text GLabel 800  1850 0    50   Input ~ 0
A2
Text GLabel 800  1950 0    50   Input ~ 0
A3
Text GLabel 800  2050 0    50   Input ~ 0
RXD
Text GLabel 800  2150 0    50   Input ~ 0
TXD
Text GLabel 800  2250 0    50   Input ~ 0
MOSI
Text GLabel 800  2350 0    50   Input ~ 0
MISO
Text GLabel 1950 1350 2    50   Input ~ 0
Free1
Text GLabel 1950 1450 2    50   Input ~ 0
D2
Text GLabel 1950 1550 2    50   Input ~ 0
D3
Text GLabel 1950 1650 2    50   Input ~ 0
D4
Text GLabel 1950 1750 2    50   Input ~ 0
D5
Text GLabel 1950 1850 2    50   Input ~ 0
D6
Text GLabel 1950 1950 2    50   Input ~ 0
D7
Text GLabel 1950 2050 2    50   Input ~ 0
D8
Text GLabel 1950 2150 2    50   Input ~ 0
Free2
Text GLabel 1950 2250 2    50   Input ~ 0
Free3
Text GLabel 1950 2350 2    50   Input ~ 0
SCK
Wire Wire Line
	800  1450 1250 1450
Wire Wire Line
	1250 1550 800  1550
Wire Wire Line
	800  1650 1250 1650
Wire Wire Line
	800  1750 1250 1750
Wire Wire Line
	1250 1850 800  1850
Wire Wire Line
	800  1950 1250 1950
Wire Wire Line
	1250 2050 800  2050
Wire Wire Line
	800  2150 1250 2150
Wire Wire Line
	1250 2250 800  2250
Wire Wire Line
	800  2350 1250 2350
Wire Wire Line
	1950 2350 1750 2350
Wire Wire Line
	1950 2250 1750 2250
Wire Wire Line
	1950 2150 1750 2150
Wire Wire Line
	1950 2050 1750 2050
Wire Wire Line
	1950 1950 1750 1950
Wire Wire Line
	1950 1850 1750 1850
Wire Wire Line
	1950 1750 1750 1750
Wire Wire Line
	1950 1650 1750 1650
Wire Wire Line
	1950 1550 1750 1550
Wire Wire Line
	1950 1450 1750 1450
Wire Wire Line
	1750 1350 1950 1350
$Comp
L power:GND #PWR0104
U 1 1 5E6C7F7E
P 5550 1300
F 0 "#PWR0104" H 5550 1050 50  0001 C CNN
F 1 "GND" V 5555 1172 50  0000 R CNN
F 2 "" H 5550 1300 50  0001 C CNN
F 3 "" H 5550 1300 50  0001 C CNN
	1    5550 1300
	0    1    1    0   
$EndComp
Text GLabel 5550 2400 0    50   Input ~ 0
Free1
Text GLabel 5550 2300 0    50   Input ~ 0
D2
Text GLabel 5550 2200 0    50   Input ~ 0
D3
Text GLabel 5550 2100 0    50   Input ~ 0
D4
Text GLabel 5550 2000 0    50   Input ~ 0
D5
Text GLabel 5550 1900 0    50   Input ~ 0
D6
Text GLabel 5550 1800 0    50   Input ~ 0
D7
Text GLabel 5550 1700 0    50   Input ~ 0
D8
Text GLabel 5550 1600 0    50   Input ~ 0
Free2
Text GLabel 5550 1500 0    50   Input ~ 0
Free3
Text GLabel 5550 1400 0    50   Input ~ 0
SCK
$Comp
L power:+5V #PWR0105
U 1 1 5E6C96DB
P 6400 1300
F 0 "#PWR0105" H 6400 1150 50  0001 C CNN
F 1 "+5V" V 6415 1428 50  0000 L CNN
F 2 "" H 6400 1300 50  0001 C CNN
F 3 "" H 6400 1300 50  0001 C CNN
	1    6400 1300
	0    1    1    0   
$EndComp
Text GLabel 6400 2300 2    50   Input ~ 0
SCL
Text GLabel 6400 2200 2    50   Input ~ 0
SDA
Text GLabel 6400 2100 2    50   Input ~ 0
A0
Text GLabel 6400 2000 2    50   Input ~ 0
A1
Text GLabel 6400 1900 2    50   Input ~ 0
A2
Text GLabel 6400 1800 2    50   Input ~ 0
A3
Text GLabel 6400 1700 2    50   Input ~ 0
RXD
Text GLabel 6400 1600 2    50   Input ~ 0
TXD
Text GLabel 6400 1500 2    50   Input ~ 0
MOSI
Text GLabel 6400 1400 2    50   Input ~ 0
MISO
$Comp
L power:+3.3V #PWR0106
U 1 1 5E6D5005
P 6400 2400
F 0 "#PWR0106" H 6400 2250 50  0001 C CNN
F 1 "+3.3V" V 6415 2528 50  0000 L CNN
F 2 "" H 6400 2400 50  0001 C CNN
F 3 "" H 6400 2400 50  0001 C CNN
	1    6400 2400
	0    1    1    0   
$EndComp
Wire Wire Line
	6400 2000 6200 2000
Wire Wire Line
	6200 1900 6400 1900
Wire Wire Line
	6400 1800 6200 1800
Wire Wire Line
	6200 1500 6400 1500
Wire Wire Line
	6200 1400 6400 1400
Wire Wire Line
	6200 2300 6400 2300
Wire Wire Line
	6200 1300 6400 1300
Wire Wire Line
	6400 2200 6200 2200
Wire Wire Line
	6200 1700 6400 1700
Wire Wire Line
	6200 2100 6400 2100
Wire Wire Line
	6400 1600 6200 1600
Wire Wire Line
	5700 1300 5550 1300
Wire Wire Line
	5550 1400 5700 1400
Wire Wire Line
	5700 1500 5550 1500
Wire Wire Line
	5550 1600 5700 1600
Wire Wire Line
	5550 2300 5700 2300
Wire Wire Line
	5700 2400 5550 2400
Wire Wire Line
	6400 2400 6200 2400
$Comp
L Connector_Generic:Conn_02x12_Top_Bottom J2
U 1 1 5E6BD7BA
P 5900 1800
F 0 "J2" H 5950 2517 50  0000 C CNN
F 1 "Conn_02x12_Top_Bottom" H 5950 2426 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x12_Pitch2.54mm_SMD" H 5900 1800 50  0001 C CNN
F 3 "~" H 5900 1800 50  0001 C CNN
	1    5900 1800
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_02x12_Top_Bottom J1
U 1 1 5E6BA616
P 1450 1850
F 0 "J1" H 1500 2567 50  0000 C CNN
F 1 "Conn_02x12_Top_Bottom" H 1500 2476 50  0000 C CNN
F 2 "Pin_Headers:Pin_Header_Straight_2x12_Pitch2.54mm_SMD" H 1450 1850 50  0001 C CNN
F 3 "~" H 1450 1850 50  0001 C CNN
	1    1450 1850
	1    0    0    -1  
$EndComp
Wire Wire Line
	5550 1700 5700 1700
Wire Wire Line
	5700 1800 5550 1800
Wire Wire Line
	5550 1900 5700 1900
Wire Wire Line
	5700 2000 5550 2000
Wire Wire Line
	5550 2100 5700 2100
Wire Wire Line
	5700 2200 5550 2200
$EndSCHEMATC
