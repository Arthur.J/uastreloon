//#FOR TRANSMITTER With some capteurs
#include <SPI.h>
#include <RH_RF95.h>
#define RFM95_CS 10
#define RFM95_RST 9
#define RFM95_INT 2
#define RF95_FREQ 434.0
#include "Arduino_SensorKit.h"
#define LED 6 
RH_RF95 rf95(RFM95_CS, RFM95_INT);
 
void setup() 
{
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);
  
  pinMode(LED,OUTPUT);    //Sets the pinMode to Output 
  Pressure.begin();
  char* typeofcapteur = "0";
  while (!Serial);
  Serial.begin(9600); 
  delay(100);
  //Define Data from carte seeed
  Environment.begin();
  Serial.println("Arduino LoRa TX Test!");
// manual reset 
  digitalWrite(RFM95_RST, LOW);
  delay(10); 
  digitalWrite(RFM95_RST, HIGH);
  delay(10);
  while (!rf95.init()) { 
    Serial.println("LoRa radio init failed"); 
    while (1);
  }
  
  Serial.println("LoRa radio init OK!");
  // Defaults after init are 434.0MHz, modulation GFSK_Rb250Fd250, +13dbM
  if (!rf95.setFrequency(RF95_FREQ)) {
    Serial.println("setFrequency failed");
    while (1);
   }
   
  Serial.print("Set Freq to: "); Serial.println(RF95_FREQ);
  
  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
  
  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then // you can set transmitter powers from 5 to 23 dBm:
  rf95.setTxPower(23, false);

  }
  

  
  void loop() 
  {
    ///////////////////////////////////////////////////////////////////////////
    //PARTIE CAPTEUR
    //Allume-Eteint la LED
    digitalWrite(LED, HIGH); //Sets the voltage to high 
    delay(1000);             //Waits for 1000 milliseconds 
    digitalWrite(LED, LOW);  //Sets the voltage to low
    delay(1000);             //Waits for 1000 milliseconds
    ///////////////////////////////////////////////////////////////////////////
    // Récupére la pression atmosphérique
    int PressureDataIn = Pressure.readPressure();
    Serial.print("Pressure: ");
    Serial.print(PressureDataIn);
    Serial.println(" Pa");
    int taillePressure = sizeof(PressureDataIn)+1;
    ///////////////////////////////////////////////////////////////////////////
    //Récupérer L'altitude
    int AltitudeDataIn = Pressure.readAltitude();
    Serial.print("Altitude : ");
    Serial.print(AltitudeDataIn);
    Serial.println(" m");
    int tailleAltitude = sizeof(AltitudeDataIn)+1;
    ///////////////////////////////////////////////////////////////////////////
    //Récupére la Température
    int TemperatureDataIn = Environment.readTemperature();
    Serial.print("Temperature : ");
    Serial.print(TemperatureDataIn); 
    Serial.println(" C");
    int tailleTemperature = sizeof(TemperatureDataIn)+1;
   ///////////////////////////////////////////////////////////////////////////
   //Initialisation des compteurs
    float tic =0;
    float toc = 0;
    tic = millis();
    ///////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////////////////////////////////////////////
    SendData(TemperatureDataIn,"1");
    SendData(AltitudeDataIn,"2");
    SendData(PressureDataIn,"3");
    ///////////////////////////////////////////////////////////////////////////
   ///////////////////////////////////////////////////////////////////////////
  // Send a message to Receiver 
  Serial.print("Sending a message to Reveiver");
  uint8_t radiopacket[] = "Hello World, rien ne sert de courir, il faut partir à point !"; 
  Serial.print("Sending..."); delay(10);
  Serial.println("Waiting for packet to complete..."); delay(10);
  rf95.waitPacketSent();
  rf95.send(radiopacket, sizeof(radiopacket));
  delay(1000);

  //Calcul Temps d'emission
  toc= millis();
  int TimeEmission = 0;
  TimeEmission = toc-tic;
  Serial.print("Le temps d'emission est ");
  Serial.print((int)TimeEmission);
  Serial.println(" ms");
  ///////////////////////////////////////////////////////////////////////////
  delay(5000); 
 
 }
 void SendData(int value_capteur,char *typeofcapteur)
  {
    String message;
    String sending;
    String valueString = String(value_capteur);
    if(typeofcapteur=="1")
      {
        message =String("Temperature : "+valueString+" °C");
        sending = String("Temperature");
      }
     else if (typeofcapteur=="2")
      {
        message= String("Altitude : "+valueString+" m");
        sending = String("Altitude");
      }
     else if (typeofcapteur=="3")
      {
        message = String("Pressure : "+valueString+" Pa");
        sending = String("Pressure");
      }
      
    int taillemessage = message.length()+1;
    char Message_str[taillemessage];
    message.toCharArray(Message_str, taillemessage);
    Serial.print("Sending..."); delay(10);
    Serial.println(sending); delay(10);
    rf95.waitPacketSent();
    rf95.send(Message_str, sizeof(Message_str));
    delay(1000);
  }
  
