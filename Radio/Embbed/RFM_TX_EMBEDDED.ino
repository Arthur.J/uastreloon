///////////////////////////////
// 08/04/2021 Version_Tx 1.0 //
// Projet Astre loon         //
// Code Ballon TX            //
///////////////////////////////

//Inclusion des bibliothèques
#include <SPI.h> 
#include <RH_RF95.h>
#include "DHT.h"

//Configuration des pins de communication.
#define RFM95_CS 10 //Pin de Chip Select
#define RFM95_RST 9 //Pin de reset
#define RFM95_INT 2 //
#define LED 3 //Led
#define LED_tx 0 //Led
#define DHTPIN 4
#define DHTTYPE DHT22 
DHT dht(DHTPIN, DHTTYPE);
// Change to 434.0 or other frequency, must match RX's freq! 
#define RF95_FREQ 434.0

//Configuration de la radio.
RH_RF95 rf95(RFM95_CS, RFM95_INT);

void setup() 
{
  // Reset de la carte RFM
  pinMode(RFM95_RST, OUTPUT);
  digitalWrite(RFM95_RST, HIGH);
  delay(100);
  digitalWrite(RFM95_RST, LOW);
  delay(10); 
  digitalWrite(RFM95_RST, HIGH);
  delay(10);

  //Initialisation des LED
  pinMode(LED, OUTPUT);
  digitalWrite(LED,LOW);
  pinMode(LED_tx,OUTPUT);
  digitalWrite(LED_tx,LOW);

  char* typeofcapteur = "0";
  dht.begin();
  
  // Defaults after init are 434.0MHz, 13dBm, Bw = 125 kHz, Cr = 4/5, Sf = 128chips/symbol, CRC on
  // The default transmitter power is 13dBm, using PA_BOOST.
  // If you are using RFM95/96/97/98 modules which uses the PA_BOOST transmitter pin, then // you can set transmitter powers from 5 to 23 dBm:
  rf95.init();
  rf95.setFrequency(RF95_FREQ);
  rf95.setTxPower(23, false);
}


void loop() 
{

   float PressureDataIn = dht.readTemperature();;
   float TemperatureDataIn = dht.readTemperature();
  //Allumage de Led pour indiquer qu'on boucle bien. 
  digitalWrite(LED,HIGH);
  delay(1000);
  digitalWrite(LED,LOW);
  delay(1000);
  

  digitalWrite(LED_tx,HIGH); //Allumage de la led indiquant l'émission d'un message
   // Send a message to Receiver 
 
    SendData(TemperatureDataIn,"1");
    SendData(PressureDataIn,"3");
  String message = "Hello World, rien ne sert de courir, il faut partir à point !";
  int taillemessage = message.length()+1;
  char Message_str[taillemessage];
  message.toCharArray(Message_str, taillemessage);
  delay(10);delay(10);
  rf95.waitPacketSent();
  rf95.send(Message_str, sizeof(Message_str));
  delay(1000);
  
  digitalWrite(LED_tx,LOW); //Extinction de la led.
  delay(2000); 
}
 void SendData(float value_capteur,char *typeofcapteur)
  {
    String message;
    String sending;
    String valueString = String(value_capteur);
    if(typeofcapteur=="1")
      {
        message =String("Temperature : "+valueString+" °C");
        sending = String("Temperature");
      }
     else if (typeofcapteur=="2")
      {
        message= String("Altitude : "+valueString+" m");
        sending = String("Altitude");
      }
     else if (typeofcapteur=="3")
      {
        message = String("Pressure : "+valueString+" Pa");
        sending = String("Pressure");
      }
      
    int taillemessage = message.length()+1;
    char Message_str[taillemessage];
    message.toCharArray(Message_str, taillemessage);
    delay(10);
    delay(10);
    rf95.waitPacketSent();
    rf95.send(Message_str, sizeof(Message_str));
    delay(1000);
  }
